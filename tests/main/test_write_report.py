import os.path
from glob import glob
from datetime import date
import pandas as pd
from pandas.util.testing import assert_frame_equal
from main import write_report


def test_runs(tmpdir):
    """Just check if function executes without errors"""
    data_frame = pd.DataFrame(
        columns=pd.MultiIndex.from_product([['S1', 'S2'], ['A', 'B']]),
        index=[date(2000, 1, 21), date(2000, 1, 22), date(2000, 1, 23),
               date(2000, 1, 25), date(2000, 1, 26)],
        data = [
            [0.2, 0.5, 0.0, 1.0],
            [0.2, 0.5, 0.0, 1.0],
            [0.0, 1.0, 1.0, 1.0],
            [0.4, 0.5, 0.6, 1.0],
            [1.0, 1.0, 0.0, 0.0],
        ]
    )
    # Windows + Python 3.6 path encoding problems seem to affect 'py' module, so just use os.path.join
    write_report(data_frame, os.path.join(tmpdir, 'out.xls'))
