from collections import OrderedDict
from datetime import date
import pytest
import pandas as pd
from pandas.util.testing import assert_frame_equal
from main import stack_data_frames


cases = [
    (
        [
            pd.DataFrame(
                columns=['A', 'B'],
                index=['C', 'D'],
                data=[
                    [0, 1],
                    [2, 3],
                ],
            ),
            pd.DataFrame(
                columns=['A', 'B'],
                index=['C', 'D'],
                data=[
                    [4, 5],
                    [6, 7],
                ],
            ),
        ],
        ['first', 'second'],
        pd.DataFrame(
            data=[
                [0, 2, 1, 3],
                [4, 6, 5, 7],
            ],
            index=['first', 'second'],
            columns=pd.MultiIndex.from_product([['A', 'B'], ['C', 'D']])
        )
    ),
    (
        [
            pd.DataFrame(
                columns=['A', 'B'],
                index=['C', 'D'],
                data=[
                    [0, 1],
                    [2, 3],
                ],
            ),
        ],
        ['single'],
        pd.DataFrame(
            data=[
                [0, 2, 1, 3],
            ],
            index=['single'],
            columns=pd.MultiIndex.from_product([['A', 'B'], ['C', 'D']])
        )
    ),
    (
        [
            pd.DataFrame(columns=['A'], index=['C', 'D'], data=[[0], [1]]),
            pd.DataFrame(columns=['A'], index=['C', 'D'], data=[[2], [3]]),
            pd.DataFrame(columns=['A'], index=['C', 'D'], data=[[4], [5]]),
            pd.DataFrame(columns=['A'], index=['C', 'D'], data=[[6], [7]]),
        ],
        [10, 11, 12, 13],
        pd.DataFrame(data=[[0, 1], [2, 3], [4, 5], [6, 7]],
            index=[10, 11, 12, 13],
            columns=pd.MultiIndex.from_product([['A'], ['C', 'D']])
        )
    ),
    (
        [
            pd.DataFrame(columns=[10], index=[100], data=[['a']]),
            pd.DataFrame(columns=[10], index=[100], data=[['b']]),
            pd.DataFrame(columns=[10], index=[100], data=[['c']]),
            pd.DataFrame(columns=[10], index=[100], data=[['d']]),
        ],
        [10, 11, 12, 13],
        pd.DataFrame(data=[['a'], ['b'], ['c'], ['d']],
            index=[10, 11, 12, 13],
            columns=pd.MultiIndex.from_product([[10], [100]])
        )
    ),
]


@pytest.mark.parametrize("frames,indexes,expected", cases)
def test_return_value(frames, indexes, expected):
    """Check whether the function returns expected results for given inputs."""
    assert_frame_equal(stack_data_frames(frames, indexes), expected)


@pytest.mark.parametrize("name", ['A name', '   ', '"', '', None])
def test_assign_index_name(name):
    """Check whether result index name is properly assigned to index_name argument value."""
    frame = stack_data_frames(cases[0][0], cases[0][1], index_name=name)
    assert frame.index.name == name
