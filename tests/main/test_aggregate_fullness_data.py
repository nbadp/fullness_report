import os.path
from glob import glob
from datetime import date
import pandas as pd
from pandas.util.testing import assert_frame_equal
from main import aggregate_fullness_data


data_dir = os.path.join(os.path.dirname(__file__), 'test_aggregate_fullness_data')


def test_result():
    """Check result for predefined input files. Single case. Using default params."""
    glob_pattern = os.path.join(data_dir, '*.csv')
    files = glob(glob_pattern)
    expected = pd.DataFrame(
        columns=pd.MultiIndex.from_product([['S1', 'S2'], ['A', 'B']]),
        index=[date(2000, 1, 21), date(2000, 1, 22), date(2000, 1, 23),
               date(2000, 1, 25), date(2000, 1, 26)],
        data = [
            [0.2, 0.5, 0.0, 1.0],
            [0.2, 0.5, 0.0, 1.0],
            [0.0, 1.0, 1.0, 1.0],
            [0.4, 0.5, 0.6, 1.0],
            [1.0, 1.0, 0.0, 0.0],
        ]
    )
    assert_frame_equal(aggregate_fullness_data(files), expected, check_names=False)


def test_opt_arguments():
    """Function should return same results for inputs that differ only in csv separator or date format.
    Assuming said arguments match input files.
    """
    separators ={'comma': ',', 'period': '.', 'semicolon': ';'}
    results = []
    for sep in separators:
        glob_pattern = os.path.join(data_dir, '*.csv.' + sep)
        date_format = '%Y-%m-%d.csv.' + sep
        files = glob(glob_pattern)
        results.append(aggregate_fullness_data(files, csv_sep=separators[sep], date_format=date_format))
    expected = results[0]
    for result in results[1:]:
        assert_frame_equal(result, expected)
