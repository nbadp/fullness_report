from collections import OrderedDict
import pytest
import pandas as pd
from pandas.util.testing import assert_frame_equal
from main import calculate_fullness_by_class


cases = [
    (
        pd.DataFrame(OrderedDict([
            ('id-like',[0, 1, 2]),
            ('Class', ['PC', 'Cat', 'Cat']),
            ('Full', [1, 10, 100]),
            ('Whatever', [1, 0, 100]),
            ('Empty', [0, 0, 0]),
        ])),
        pd.DataFrame(OrderedDict([
            ('Full', [1.0, 1.0]),
            ('Whatever', [0.5, 1.0]),
            ('Empty', [0.0, 0.0]),
        ]), index=['Cat', 'PC'])
    ),
    (
        pd.DataFrame(OrderedDict([
            ('id-like',[0, 1, 2, 3, 4, 5]),
            ('Class', ['A', 'B', 'C', 'D', 'E', 'F']),
            ('Full', [1, 10, 100, 4, 17, 1]),
            ('Some', [0, 0, 1000, 0, 0, 1]),
            ('All but one', [14, 1, 1000, 1, 0, 1]),
            ('Only one', [1, 0, 0, 0, 0, 0]),
            ('Empty', [0, 0, 0, 0, 0, 0]),
        ])),
        pd.DataFrame(OrderedDict([
            ('Full', [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]),
            ('Some', [0.0, 0.0, 1.0, 0.0, 0.0, 1.0]),
            ('All but one', [1.0, 1.0, 1.0, 1.0, 0.0, 1.0]),
            ('Only one', [1.0, 0.0, 0.0, 0.0, 0.0, 0.0]),
            ('Empty', [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]),
        ]), index=['A', 'B', 'C', 'D', 'E', 'F'])
    ),
    (
        pd.DataFrame(OrderedDict([
            ('id-like',[0, 1, 2, 3, 4]),
            ('Class', ['S', 'S', 'S', 'S', 'S']),
            ('Full', [1, 10, 100, 4, 17]),
            ('Some', [0, 0, 1000, 0, 1]),
            ('All but one', [14, 1, 1000, 1, 0]),
            ('Only one', [1, 0, 0, 0, 0]),
            ('Empty', [0, 0, 0, 0, 0]),
        ])),
        pd.DataFrame(OrderedDict([
            ('Full', [1.0]),
            ('Some', [0.4]),
            ('All but one', [0.8]),
            ('Only one', [0.2]),
            ('Empty', [0.0]),
        ]), index=['S'])
    ),
]


@pytest.mark.parametrize("input,expected", cases)
def test_return_value(input, expected):
    """Check whether the function returns expected results for given inputs. Index names
    are not checked since they aren't important and will bloat expected data frame creation.
    """
    assert_frame_equal(calculate_fullness_by_class(input), expected, check_names=False)
