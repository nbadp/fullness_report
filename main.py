import pandas as pd
import os
import os.path
from glob import glob
from datetime import datetime
import xlsxwriter


def _read_csv(path, *args, **kwargs):
    """A crude workaround for https://github.com/pandas-dev/pandas/issues/15086

    On windows using Python >= 3.6 pandas.read_csv can't handle non-ascii paths.
    """
    with open(path, 'r') as f:
        return pd.read_csv(f, *args, **kwargs)


def calculate_fullness_by_class(data_frame):
    """Given DataFrame with stock data, calculate fullness per item class for each storage.

    Here fullness is calculated as number of item types of given class with non-zero stock
    divided by total number of item types in given class.

    Args:
        data_frame: DataFrame containing remaining stock information with columns structure like
            id, class, storage 1, ..., storage N. Class column is expected to be named 'Class'.
            Id column is discarded, but each row is is expected to correspond to unique item id.
            Storage columns labels are preserved.

    Returns:
        DataFrame containing floating point fullness values with storage names as column
        labels and goods classes as row labels.
    """
    fullness = data_frame.groupby('Class').apply(lambda column: (column > 0).sum() / column.count())
    fullness.drop(fullness.columns[:2], axis=1, inplace=True)
    fullness.columns.rename('Storage', inplace=True)
    return fullness


def stack_data_frames(frames, indexes, index_name=None):
    """Stack iterable of simple DataFrames into single DataFrame with multi level columns.

    Args:
        frames: Iterable of DataFrames with same shape and no MultiIndex.
        indexes: Iterable of row indexes corresponding to the frames.
        index_name: Name for the resulting DataFrame index.

    Returns:
        DataFrame where each row corresponds to input data frame collapsed into MultiIndex
        Sequence, with original frames index being inner level of new frame columns.
    """
    rows = [frame.unstack().rename(index) for frame, index in zip(frames, indexes)]
    result = pd.concat(rows, axis=1).transpose()
    if index_name is not None:
        result.index.rename(index_name, inplace=True)
    return result


def aggregate_fullness_data(files, csv_sep=',', date_format='%Y-%m-%d.csv'):
    """Given .csv files corresponding to dates, build DataFrame with fullness data for each date.

    For each file calculate_fullness_by_class is run, resulting DataFrame is collapsed into
    MultiIndex Sequence and used as a row with date extracted from file name as index.
    See calculate_fullness_by_class docstring for details.

    Args:
        files: Iterable of csv file paths to process. Filenames are expected to match date_format.
        csv_sep: Separator used in the scv files. Passed to pandas.read_csv.
        date_format: Should match filename with extension. Passed to datetime.strptime as is.

    Returns:
        DataFrame with dates as index and double level columns. Top level is storage names and bottom
        level is goods classes.
    """
    frames, dates = [], []
    for path in files:
        fullness_data = calculate_fullness_by_class(_read_csv(path, sep=csv_sep))
        filename = os.path.basename(path)
        date = datetime.strptime(filename, date_format).date()
        frames.append(fullness_data)
        dates.append(date)
    return stack_data_frames(frames, dates, index_name='Date')


def write_report(data_frame, filename, date_format='dd-mm'):
    """Generate .xlsx report with charts from DataFrame produced by aggregate_fullness_data.

    Each storage column is represented by a chart on a separate sheet. Graphs for each goods class are
    plotted on each chart.
    """
    writer = pd.ExcelWriter(filename, engine='xlsxwriter', date_format=date_format)
    workbook = writer.book
    for storage in data_frame.columns.levels[0]:
        df = data_frame[storage]
        (df * 100).to_excel(writer, sheet_name=storage)
        worksheet = writer.sheets[storage]
        chart = workbook.add_chart({'type': 'line'})
        for i, col in enumerate(df.columns):
            num_points = len(df)
            chart.add_series({
                'categories': [storage, 1, 0, num_points, 0],
                'values':     [storage, 1, i+1, num_points, i+1],
                'name': col,
            })
        chart.set_size({'x_scale': 2, 'y_scale': 2})
        chart.set_x_axis({
            'name': 'Дата',
            'date_axis': True,
            'major_gridlines': {'visible': True}
        })
        chart.set_y_axis({
            'name': 'Процент',
        })
        worksheet.insert_chart('A1', chart)
    writer.save()


def main():
    input_folder = 'остатки\\'
    output_folder = 'отчеты\\'
    number_of_dates_range = range(5, 21)

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    files = glob(os.path.join(input_folder, '*.csv'))
    frame = aggregate_fullness_data(files, csv_sep=';')
    for number_of_dates in number_of_dates_range:
        end_date = frame.index[number_of_dates - 1]
        filename = str(end_date) + '.xlsx'
        output_path = os.path.join(output_folder, filename)
        write_report(frame[0:number_of_dates], output_path)


if __name__ == '__main__':
    main()
